# CUDAサンプルプログラム（画像処理）



## 目的と機能

* 目的
  * CUDA C/C++で画像処理を行う手順を示す。
  * CUDAとCPU比較するために、OpenMP(CPU)の画像処理も示す。
* 機能
  * 入力画像を加工した画像をファイルに出力する。
  * 実行回数を指定できる。
  * 実行所要時間と、その統計値を出力する。



## 開発環境

開発に用いる環境を以下に示す。

​	

* 共通
  * NVIDIA製 GPUで Compute Capability (CC) が 6.1 以上のもの
  * CUDA Driver (CUDA 10.0 対応版)
* Linux
  * CUDA Toolkit 10.0.*
  * Ubuntu Desktop 18.04.* 64bit版
* Windows 
  * CUDA Toolkit 10.0.*
  * Visual Studio 2017 Professional (version 15.9.*)
    * ワークロード「C++によるデスクトップ開発」
    * 個別のコンポーネント
      * デスクトップ C++用 Windows 10 SDK (10.0.0.15063.0)[x86 および x64]




## コマンドラインの仕様



- 入力画像のファイル名。画像形式はOpenCVで読めるもの。
- 出力画像ファイル名。
- オプション
  - 実装済みの画像処理の一覧を表示し、指定した画像処理だけを実行する。
  - 画像処理の実行回数。
  - CUDAのブロックサイズ
  - OpenMPの使用スレッド個数の上限

