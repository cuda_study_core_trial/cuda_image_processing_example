@echo off
rem -- nvprof (CUDAのコマンドライン・プロファイラー) でプロファイルを収集して表示する

rem -- 設定
set NVPROF="%CUDA_PATH%\bin\nvprof.exe"
set EXE=x64\Release\cuda_image_filter.exe data\sample.jpg data\sample-out --omp_ignore  --loop 10

rem -- メモ: nvprof の主なオプション
rem   オプション無し : APIごとの所要時間と実行回数を出力
rem   --metrics all : 全メトリクス値を出力
rem   --events all : 全イベント値を出力
rem   --csv : CSV形式で標準出力に出力
rem   --export-profile ファイル名 : nvvp 用のプロファイルを出力する


set PROFILE_TYPE=%1
if "%PROFILE_TYPE%" == "" (
  echo nvprof でプロファイルを計測する。
  echo usage: %0 type
  echo typeは以下の1つを指定する:
  echo   api           APIごとの所要時間と実行回数を出力
  echo   metrics_all   全メトリクス値を出力
  echo   events_all    全イベント値を出力
  exit /b 1
)

if "%PROFILE_TYPE%" == "api" (
  echo -- APIの実行所要時間
  %NVPROF% %EXE%
)

if "%PROFILE_TYPE%" == "metrics_all" (
  echo -- 全メトリクス
  %NVPROF% --metrics all %EXE%
)

if "%PROFILE_TYPE%" == "events_all" (
  echo -- 全イベント
  %NVPROF% --events all %EXE%
)

rem -- コンソール以外から実行した場合、一時停止する
if not "%SESSIONNAME%" == "Console" (
  pause
)
