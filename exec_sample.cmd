@echo off

set EXE=x64\Release\cuda_image_filter.exe
set IN_OUT=data\sample.jpg data\sample-out

  %EXE% %IN_OUT% --cuda_ignore --loop 3 --omp_nthreads=1
  %EXE% %IN_OUT%  --omp_ignore --loop 3 -x 512 -y 1


if 0 == 1 (
  %EXE% %IN_OUT% --supress_show_spec --cuda_ignore --loop 10 --omp_nthreads=1
  %EXE% %IN_OUT% --cuda_ignore --loop 10 --omp_nthreads=2
  %EXE% %IN_OUT% --cuda_ignore --loop 10 --omp_nthreads=4
  %EXE% %IN_OUT% --cuda_ignore --loop 10 --omp_nthreads=8
)

if 0 == 1 (
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 128 -y 1
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 256 -y 1
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 512 -y 1
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 1024 -y 1
)

if 0 == 1 (
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 256 -y 1
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 128 -y 2
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 64 -y 4
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 32 -y 8
)

if 0 == 1 (
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 128 -y 1
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 64 -y 2
  %EXE% %IN_OUT% --omp_ignore --loop 10 -x 32 -y 3
)

rem -- コンソール以外から実行した場合、一時停止する
if not "%SESSIONNAME%" == "Console" (
  pause
)
