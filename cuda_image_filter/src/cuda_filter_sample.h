﻿#pragma once

#include <opencv2/core.hpp>

cv::Mat Benchmark_cudaRgbToGray(const int nloops, const int block_size_x, const int block_size_y, const cv::Mat & input);
