﻿#pragma once

#include <opencv2/core.hpp>

cv::Mat Benchmark_cpuRgbToGray(const int nloops, const int nthreads, const cv::Mat & input);

