﻿#pragma once

/*
https://cpplover.blogspot.com/2014/10/cfinally.html
*/

#include <functional>

/**
 * @brief スコープなら抜けるときにコンストラクタで指定された処理を実行する。リソースの開放に用いる。
 */
class scoped_guard
{
    std::function<void()> f;

public:
    explicit scoped_guard(std::function< void() > f)
        : f(f) { }

    scoped_guard(scoped_guard const &) = delete;
    void operator = (scoped_guard const &) = delete;


    ~scoped_guard()
    {
        f();
    }
};
