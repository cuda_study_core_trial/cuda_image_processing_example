﻿#include <stdexcept>
#include <memory>
#include <iostream>
#include <cassert>
#include <cstdint>
#include <cstdlib>
#include <omp.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "cuda_utils.h"
#include "scoped_guard.h"
#include "stat_util.h"

/**
 * @brief RGB -> GRAY 1画素 (CPU/CUDA 共通)
 * 
 * @param[in] r Red(8bit)
 * @param[in] g Green(8bit)
 * @param[in] b Blue(8bit)
 * @return グレースケール値(8bit)
 */
__device__
inline uint8_t rgbToGray(const uint8_t r, const uint8_t g, const uint8_t b)
{
    // 演算誤差を減らすために、RGBが同値の場合は、算出式を使わない。
    return ((r == g) && (g == b))? (r): ((uint8_t)(0.299f * r + 0.587f * g + 0.114f * b));
}

/**
 * @brief 座標が画像の範囲内か？
 */
__device__
inline bool inRange(const size_t x, const size_t y, const size_t image_size_x, const size_t image_size_y)
{
    return (x < image_size_x) && (y < image_size_y);
}

/**
 * @brief 座標から画素のインデックスを算出する
 */
__device__
inline bool xyToIndex(const size_t x, const size_t y, const size_t input_step, const size_t number_of_components)
{
    return y * input_step + x * number_of_components;
}

/**
 * @brief RGB -> GRAY CUDAカーネル
 *
 * @param[in] image_size_x 画像サイズX
 * @param[in] image_size_y 画像サイズY
 * @param[in] input 入力画像
 * @param[in] input_step 入力画像の横幅(bytes)
 * @param[in,out] output 出力画像
 * @param[in] output_step 出力画像の横幅(bytes)
 */
__global__
void cudaRgbToGrayKernel(
    const int image_size_x, const int image_size_y,
    const uint8_t * const input, const size_t input_step,
    uint8_t * const output, const size_t output_step
)
{
    // 処理対象の画素の座標を算出する (ブロックとスレッドから決めている）
    const size_t x = threadIdx.x + blockIdx.x * blockDim.x;
    const size_t y = threadIdx.y + blockIdx.y * blockDim.y;

    // 処理対象範囲か？
    if (inRange(x, y, image_size_x, image_size_y)) {

        // 画素の座標から、入力と出力の画素のインデックスを算出する
        const size_t input_pos = xyToIndex(x, y, input_step, 3);
        const size_t output_pos = xyToIndex(x, y, output_step, 1);

        // 入力画素のRGB値を得る
        const uint8_t b = input[input_pos + 0];
        const uint8_t g = input[input_pos + 1];
        const uint8_t r = input[input_pos + 2];

        // RGBからグレースケール値を算出して、出力画素に格納する
        output[output_pos] = rgbToGray(r, g, b);
    }
}

/**
 * @brief RGB -> GRAY CUDAカーネル実行
 *
 * @param[in] block_size_x CUDAカーネル実行ブロックサイズX
 * @param[in] block_size_y CUDAカーネル実行ブロックサイズY
 * @param[in] image_size_x 画像サイズX
 * @param[in] image_size_y 画像サイズY
 * @param[in] input 入力画像
 * @param[in] input_step 入力画像の横幅(bytes)
 * @param[in,out] output 出力画像
 * @param[in] output_step 出力画像の横幅(bytes)
 */
void cudaRgbToGray(
    const int block_size_x, const int block_size_y,
    const int image_size_x, const int image_size_y,
    const uint8_t * const input, const size_t input_step, 
    uint8_t * const output, const size_t output_step
)
{
    // カーネル実行のブロックサイズ、グリッドサイズの算出
    const dim3 block_size(block_size_x, block_size_y);
    const dim3 grid_size(
        ceil_uint32(image_size_x, block_size.x),
        ceil_uint32(image_size_y, block_size.y));

    // カーネル実行
    cudaRgbToGrayKernel<<<grid_size, block_size>>>(image_size_x, image_size_y, input, input_step, output, output_step);


    // カーネル実行エラー取得
    cudaError_t cuda_status = cudaGetLastError();
    if (cuda_status != cudaSuccess) { throw MyCudaRuntimeError(cuda_status); }

    // カーネル終了待ち
    cuda_status = cudaDeviceSynchronize();
    if (cuda_status != cudaSuccess) { throw MyCudaRuntimeError(cuda_status); }
}

/**
 * @brief CUDA版カーネルのベンチマーク。
 * @detail CUDA版カーネルを指定したブロックサイズで、指定した回数実行して、実行所要時間の統計値を出力する。
 * @param[in] nloops ループ回数
 * @param[in] input 入力画像(RGB)
 * @return 出力画像(8bit gray)
 */
cv::Mat Benchmark_cudaRgbToGray(const int nloops, const int block_size_x, const int block_size_y, const cv::Mat & input)
{
    std::cout << "cudaRgbToGray: "
        << " block_size=" << block_size_x << "," << block_size_y
        << std::endl;

    // 出力先
    cv::Mat output = cv::Mat(input.rows, input.cols, CV_8UC1);

    // 画像処理実行、実行所要時間計測
    std::vector<double> elapsed;
    for (int i = 0; i < nloops; ++i) {
        // 実行所要時間計測開始
        const auto time_start_sec = omp_get_wtime();	// OpenMP: wall clock timeを取得
        {
            cudaError_t cuda_status = cudaSuccess;

            // GPUに入力画像の領域を確保する
            const size_t input_array_size = input.step * input.rows;
            uint8_t *device_input = nullptr;
            cuda_status = cudaMalloc(&device_input, input_array_size);
            if (cuda_status != cudaSuccess) { throw MyCudaRuntimeError(cuda_status); }
            scoped_guard guard_device_input([device_input] { cudaFree(device_input); });	// スコープ外に出たらGPUのメモリを解放

            // GPUに出力画像の領域を確保する。
            const size_t output_array_size = output.step * output.rows;
            uint8_t *device_output = nullptr;
            cuda_status = cudaMalloc(&device_output, output_array_size);
            if (cuda_status != cudaSuccess) { throw MyCudaRuntimeError(cuda_status); }
            scoped_guard guard_device_output([device_output] { cudaFree(device_output); });	// スコープ外に出たらGPUのメモリを解放

            // 入力画像をGPUにコピーする
            cuda_status = cudaMemcpy(device_input, input.data, input_array_size, cudaMemcpyHostToDevice);
            if (cuda_status != cudaSuccess) { throw MyCudaRuntimeError(cuda_status); }

            // 画像処理を実行
            cudaRgbToGray(
                block_size_x, block_size_y,
                input.cols, input.rows,
                device_input, input.step,
                device_output, output.step
            );

            // GPUから出力画像をCPUにコピーする。
            cuda_status = cudaMemcpy(output.data, device_output, output_array_size, cudaMemcpyDeviceToHost);
            if (cuda_status != cudaSuccess) { throw MyCudaRuntimeError(cuda_status); }
        }
        // 実行所要時間計測終了
        const auto time_end_sec = omp_get_wtime();	// OpenMP: wall clock timeを取得
        const auto elapsed_millisec = (time_end_sec - time_start_sec) * 1000;
        elapsed.push_back(elapsed_millisec);
    }

    // 実行所要時間の統計値を出力
    const auto min = stat_util::min(elapsed);
    const auto max = stat_util::max(elapsed);
    const auto mean = stat_util::mean(elapsed);
    const auto median = stat_util::median(elapsed);
    const auto var = stat_util::variance(elapsed);
    const auto std = stat_util::stddev_p(elapsed);
    std::cout << "cudaRgbToGray: elapsed[ms]"
        << " n=" << nloops
        << " min=" << min
        << " max=" << max
        << " mean=" << mean
        << " median=" << median
        << " var=" << var
        << " std=" << std
        << std::endl;
    std::cout << "cudaRgbToGray: lap[ms]" << std::endl;
    for (const auto &x : elapsed) {
        std::cout << x << std::endl;
    }

    return output;
}
