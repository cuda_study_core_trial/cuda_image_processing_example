﻿#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cmath>
#include "stat_util.h"

namespace stat_util {

/**
 * maximum
 */
double max(const std::vector<double> & data)
{
    return *std::max_element(std::begin(data), std::end(data));
}

/**
 * minimum
 */
double min(const std::vector<double> & data)
{
    return *std::min_element(std::begin(data), std::end(data));
}

/**
 * summation
 */
double sum(const std::vector<double> & data)
{
    return std::accumulate(std::begin(data), std::end(data), 0);
}

/**
 * mean value
 */
double mean(const std::vector<double> & data)
{
    return std::accumulate(std::begin(data), std::end(data), 0.0) / data.size();
}

/**
 * variance
 */
double variance(const std::vector<double> & data)
{
    double sum = 0.0;
    double sum2 = 0.0;
    for (const auto &x : data) {
        sum += x;
        sum2 += x * x;
    }

    const double n = static_cast<double>(data.size());
    const double ave = sum / n;
    const double var = sum2 / n - ave * ave;

    return var;
}

/**
 * population standard deviation
 */
double stddev_p(const std::vector<double> & data)
{
    return std::sqrt(variance(data));
}

/**
 * median
 */
double median(const std::vector<double> & data)
{
    std::vector<double> tmp(std::begin(data), std::end(data));
    std::sort(std::begin(tmp), std::end(tmp));
    const size_t median_index = tmp.size() / 2;
    const double median = ((tmp.size() % 2) == 0) ?
        static_cast<double>(tmp[median_index] + tmp[median_index - 1]) / 2:
        tmp[median_index];
    return median;
}

} // namespace
