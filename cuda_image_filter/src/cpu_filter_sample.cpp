﻿#include <stdexcept>
#include <memory>
#include <iostream>
#include <cassert>
#include <cstdint>
#include <cstdlib>
#include <omp.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "cpu_filter_sample.h"
#include "stat_util.h"

/**
 * @brief RGB -> GRAY 1画素 (CPU/CUDA 共通)
 *
 * @param[in] r Red(8bit)
 * @param[in] g Green(8bit)
 * @param[in] b Blue(8bit)
 * @return グレースケール値(8bit)
 */
inline uint8_t rgbToGray(const uint8_t r, const uint8_t g, const uint8_t b)
{
    // 演算誤差を減らすために、RGBが同値の場合は、算出式を使わない。
    return ((r == g) && (g == b)) ? (r) : ((uint8_t)(0.299f * r + 0.587f * g + 0.114f * b));
}

/**
 * @brief RGB -> GRAY  (CPU OpenMP版)
 *
 * @param[in] nthreads OpenMPで同時に実行するワーカスレッドの個数
 * @param[in] input 入力画像(RGB)を指すポインタ
 * @param[in] input_step 入力画像(RGB)の1行のデータ長[byte]
 * @param[in,out] output 出力画像(gray 8bit)を指すポインタ
 * @param[in] output_step 出力画像(gray 8bit)の1行のデータ長[byte]
 * @param[in] image_size_x 画像の横の大きさ[pixel]
 * @param[in] image_size_y 画像の縦の大きさ[pixel]
*/
void cpuRgbToGray(
    const int nthreads,
    const uint8_t * const input, const size_t input_step,
    uint8_t * const output, const size_t output_step,
    const int image_size_x, const int image_size_y
)
{
    // OpenMPで用いるスレッドの個数を設定する
    omp_set_num_threads(nthreads);

    // OpenMPで並列処理する
    #pragma omp parallel for				// OpenMP: ループを並列処理する
    for (int y = 0; y < image_size_y; ++y)  // OpenMP: ループ変数は符号付き型にする必要がある
    {
        for (int x = 0; x < image_size_x; ++x) {
            // 処理対象の画素(x,y)に対する処理

            // 画素の座標から、入力と出力の画素のインデックスを算出する
            const size_t input_pos = y * input_step + x * 3;
            const size_t output_pos = y * output_step + x;
            
            // 入力画素のRGB値を得る
            const uint8_t b = input[input_pos + 0];
            const uint8_t g = input[input_pos + 1];
            const uint8_t r = input[input_pos + 2];

            // RGBからグレースケール値を算出して、出力画素に格納する
            output[output_pos] = rgbToGray(r, g, b);
        }
    }
}

/**
 * @brief CPU版 実行ドライバ
 *
 * @param[in] nloops ループ回数
 * @param[in] nthreads スレッドの個数
 * @param[in] input 入力画像(RGB)
 * @return 出力画像(8bit gray)
 */
cv::Mat Benchmark_cpuRgbToGray(const int nloops, const int nthreads, const cv::Mat & input)
{
    std::cout << "cpuRgbToGray:"
        << " nthreads=" << nthreads << std::endl;

    // 出力先
    cv::Mat output = cv::Mat(input.rows, input.cols, CV_8UC1);

    // 画像処理実行、実行所要時間計測
    std::vector<double> elapsed;
    for (int i = 0; i < nloops; ++i) {
        // 実行所要時間計測開始
        const auto time_start_sec = omp_get_wtime();	// OpenMP: wall clock timeを取得

        // OpenMPで画像処理を実行
        cpuRgbToGray(
            nthreads,
            input.data, input.step,
            output.data, output.step,
            input.cols, input.rows
        );

        // 実行所要時間計測終了
        const auto time_end_sec = omp_get_wtime();	// OpenMP: wall clock timeを取得
        const auto elapsed_millisec = (time_end_sec - time_start_sec) * 1000;
        elapsed.push_back(elapsed_millisec);
    }

    // 実行所要時間の統計値を出力
    const auto min = stat_util::min(elapsed);
    const auto max = stat_util::max(elapsed);
    const auto mean = stat_util::mean(elapsed);
    const auto median = stat_util::median(elapsed);
    const auto var = stat_util::variance(elapsed);
    const auto std = stat_util::stddev_p(elapsed);
    std::cout << "cpuRgbToGray: elapsed[ms]"
        << " n=" << nloops
        << " min=" << min
        << " max=" << max
        << " mean=" << mean
        << " median=" << median
        << " var=" << var
        << " std=" << std
        << std::endl;
    std::cout << "cpuRgbToGray: lap[ms]" << std::endl;
    for (const auto &x : elapsed) {
        std::cout << x << std::endl;
    }
    
    return output;
}
