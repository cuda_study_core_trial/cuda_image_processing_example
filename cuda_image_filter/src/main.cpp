﻿#include <cstdint>
#include <cstdlib>
#include <cassert>

#include <stdexcept>
#include <memory>
#include <iostream>
#include <string>
#include <vector>
#include <regex>

#include <omp.h>

#include <opencv2/highgui.hpp>

#include "cmdline.h"

#include "cuda_utils.h"
#include "scoped_guard.h"

#include "cpu_filter_sample.h"
#include "cuda_filter_sample.h"


/**
 * ベンチマーク名の一覧
 */
std::string benchmark_names = \
    "cpuRgbToGray" \
    ",cudaRgbToGray" \
    ;

/**
 * コマンドライン引数
 */
struct command_args {
    bool is_help;      ///< ヘルプを表示して終了
    bool is_show_spec; ///< OpenMP や CUDA のデバイス使用を表示しない
    int loop;          ///< サンプルの実行回数
    bool omp_ignore;    ///< OpenMP版を実行しない
    int omp_nthreads;		///< max threads of OpenMP
    bool cuda_ignore;    ///< CUDA版を実行しない
    int cuda_block_size_x;	///< block_size.x of CUDA kernel
    int cuda_block_size_y;	///< block_size.y of CUDA kernel
    std::string benchmark;  ///< selected benchmark names.
    std::string input_file;	///< input image file
    std::string output_node;	///< output image file node
    bool is_specify_file;   ///< input image file and output image file node are specified.

    /** 初期値を設定 */
    command_args() :
        is_help(false),
        is_show_spec(false),
        loop(0),
        omp_ignore(false),
        omp_nthreads(0),
        cuda_ignore(false),
        cuda_block_size_x(0),
        cuda_block_size_y(0),
        is_specify_file(false),
        benchmark(benchmark_names)
        {}
};

/**
 * 文字列をデリミタ(正規表現で指定)で区切って返す。
 *
 * @see https://stackoverflow.com/questions/9435385/split-a-string-using-c11
 */
std::vector<std::string> split(const std::string& input, const std::string& regex_delimiter) {
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex_delimiter);
    std::sregex_token_iterator first{ input.begin(), input.end(), re, -1 };
    std::sregex_token_iterator last;

    std::vector<std::string> result = { first, last };
    if ((result.size() == 1) && (result[0].length() == 0)) {
        // 要素=1個で、空文字列""の場合、結果は 0要素 とする。
        return std::vector<std::string>(0);
    }
    else {
        return result;
    }
}

/**
 * コンテナに、指定した要素が含まれていれば trueを返す。
 *
 * @see https://code.i-harness.com/ja/q/8b802
 */
template <class Container>
const bool contains(const Container & container, const typename Container::value_type & element)
{
    return std::find(std::begin(container), std::end(container), element) != std::end(container);
}


/**
 * コマンドライン引数のヘルプを出力する
 *
 * @param[in] argv0 プログラム名
 */
void show_help(const char * const argv0)
{
    using std::endl;

    std::cout
        << argv0 << " [option] input_image_file output_node [option]" << endl
        << endl
        << "--help, -h : show help message" << endl
        << "--loop=N, -l N : number of loop. default=1" << endl
        ;

    std::cout << "--benchmark=benchmark1,... -b benchmark1,.. : invoke specified benchmarks (comma separated)." << endl;
    for (auto &name : split(benchmark_names, ",")) {
        std::cout << "  " << name << endl;
    }

    std::cout
        << "CPU(OpenMP):" << endl
        << "  --omp_ignore : ignore to invoke cpu(OpenMP) samples." << endl
        << "  --omp_nthreads=N, -n N: max threads of OpenMP. default=1" << endl
        << endl
        << "CUDA:" << endl
        << "  --cuda_ignore : ignore to invoke CUDA samples." << endl
        << "  --cuda_block_size_x=X, -x X: block_size.x of CUDA kernel" << endl
        << "  --cuda_block_size_y=X, -y Y: block_size.y of CUDA kernel" << endl
        ;
}

/**
 * コマンドラインを解析する
 *
 * @param[in] argc
 * @param[in] argv
 * @return コマンドライン解析結果
 */
command_args parse_commandline_args(int argc, char *argv[])
{
    // コマンドラインの仕様を定義 (長い引数名、短い引数名、説明、必須か?、デフォルト値）
    cmdline::parser p;
    p.add("help", 'h', "");
    p.add("show_spec", '\0', "");
    p.add<int>("loop", 'l', "", false, 1);
    p.add("omp_ignore", '\0', "");
    p.add<int>("omp_nthreads", 'n', "", false, 1);
    p.add("cuda_ignore", '\0', "");
    p.add<int>("cuda_block_size_x", 'x', "", false, 128);
    p.add<int>("cuda_block_size_y", 'y', "", false, 1);
    p.add<std::string>("benchmark", 'b', "", false);

    // コマンドラインを解析
    command_args args;
    if (!p.parse(argc, argv))
    {
        throw std::runtime_error("ERROR: command line args is invalid.");
    }

    // オプション引数を解析
    if (p.exist("help"))
    {
        return args;
    }
    args.is_help = p.exist("help");
    args.is_show_spec = p.exist("show_spec");
    args.loop = p.get<int>("loop");
    args.omp_ignore = p.exist("omp_ignore");
    args.omp_nthreads = p.get<int>("omp_nthreads");
    args.cuda_ignore = p.exist("cuda_ignore");
    args.cuda_block_size_x = p.get<int>("cuda_block_size_x");
    args.cuda_block_size_y = p.get<int>("cuda_block_size_y");
    args.benchmark = p.get<std::string>("benchmark");

    // オプション以外の引数を解析
    auto files = p.rest();
    if (files.size() == 2) {
        args.input_file = files[0];
        args.output_node = files[1];
        args.is_specify_file = true;
    }

    // 解析結果を返す
    return args;
}

/**
 * @brief 実行環境のOpenMPの仕様を出力する
 */
void OpenMPPutSpecification()
{
    std::cout
        << "_OPENMP=" << _OPENMP
        << " omp_get_max_threads()=" << omp_get_max_threads()
        << std::endl;
}

/**
 * @brief 実行環境のCUDA GPUの仕様を表示する
 */
void CudaPutSpecification()
{
    int device_no = -1;
    cudaGetDevice(&device_no);

    cudaDeviceProp prop;
    const auto cuda_status = cudaGetDeviceProperties(&prop, device_no);
    if (cuda_status != cudaSuccess) { throw MyCudaRuntimeError(cuda_status); }

    std::cout << "cudaGetDeviceProperties:"
        << " cc=" << prop.major << "." << prop.minor
        << " totalGlobalMem[MB]=" << (prop.totalGlobalMem / 1e6)
        << " multiProcessorCount=" << prop.multiProcessorCount
        << " name=" << prop.name
        << std::endl;
}


/**
 * CUDAの使用を開始する
 */
void CudaInitialize()
{
    cudaError_t cuda_status = cudaSetDevice(0);
    if (cuda_status != cudaSuccess) { throw MyCudaRuntimeError(cuda_status); }
}

/**
 * CUDAの使用を終了する。CUDAプロファイラ使用時には集計を行う。
 */
void CudaFinalize()
{
    cudaError_t cuda_status = cudaDeviceReset();
    if (cuda_status != cudaSuccess) { throw MyCudaRuntimeError(cuda_status); }
}

/**
 * @brief 画像処理を実行する
 * @detail
 * - 変換処理の速度比較のために、CPU版(OpenMP)と CUDA版を実行し、それぞれの所要時間をms単位で出力する。
 * - 変換結果の画像は、 CPU版は「指定ファイル名-cpu.png」に出力する。CUDA版は「指定ファイル名-cuda.png」に出力する。
 */
void ExecuteImageProcessing(int argc, char *argv[])
{
    // コマンドライン引数を解析する
    const auto args = parse_commandline_args(argc, argv);

    // ファイルが指定されていない場合は、ヘルプと、OpenMP/CUDAの仕様を表示して、終了する。
    if (args.is_help)
    {
        // ファイルが指定されていない場合は、ヘルプを表示して、終了する。
        show_help(argv[0]);
        return;
    }

    // OpenMPとCUDAの仕様の出力が要求されたか？
    if (args.is_show_spec)
    {
        OpenMPPutSpecification();
        CudaPutSpecification();
    }

    // 入力ファイル名が指定されていなかったら、終了する。
    if (!args.is_specify_file) {
        return;
    }

    // 画像を読み込む
    // https://docs.opencv.org/3.4.3/d4/da8/group__imgcodecs.html
    auto image = cv::imread(args.input_file, cv::IMREAD_UNCHANGED);
    if (image.data == nullptr)
    {
        throw std::runtime_error(std::string("ERROR: Can't read file '") + args.input_file + "'");
    }
    std::cout << "input_file=" << args.input_file
        << " size=(" << image.size().width << "," << image.size().height << ")"
        << " channels=" << image.channels()
        << std::endl;


    // コマンドラインで指定したベンチマーク項目を得る
    const auto selected_benchmarks = split(args.benchmark, ",");
    const auto select_all = selected_benchmarks.empty();


    // CPU(OpenMP)版を選択した場合
    if (!args.omp_ignore)
    {
        // 画像処理を実行し、結果画像をファイルに出力する
        if (select_all || contains(selected_benchmarks, "cpuRgbToGray"))
        {
            const auto result_image = Benchmark_cpuRgbToGray(args.loop, args.omp_nthreads, image);
            const auto output_file = std::string(args.output_node) + "-gray-cpu1.png";
            if (!cv::imwrite(output_file.c_str(), result_image))
            {
                throw std::runtime_error(std::string("ERROR: Can't write file '") + output_file + "'");
            }
        }
        // 以下、処理を追加するなら同様にする。

    }

    // CUDA版を選択した場合
    if (!args.cuda_ignore)
    {
        // CUDA GPUを初期化し、開放処理を予約
        CudaInitialize();
        scoped_guard guard_finalize_cuda([] { CudaFinalize(); });	// スコープ外に出たらGPUを解放

        // 画像処理を実行し、結果画像をファイルに出力する
        if (select_all || contains(selected_benchmarks, "cudaRgbToGray"))
        {
            const auto result_image = Benchmark_cudaRgbToGray(args.loop, args.cuda_block_size_x, args.cuda_block_size_y, image);
            const auto output_file = std::string(args.output_node) + "-gray-cuda1.png";
            if (!cv::imwrite(output_file.c_str(), result_image))
            {
                throw std::runtime_error(std::string("ERROR: Can't write file '") + output_file + "'");
            }
        }
        // 以下、処理を追加するなら同様にする。
    }
}

/**
 * @brief CPU/CUDA 比較用main
 */
int main(int argc, char *argv[])
{
    try
    {
        ExecuteImageProcessing(argc, argv);
        return EXIT_SUCCESS;
    }
    catch (MyCudaRuntimeError & exp)
    {
        std::cerr << exp.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (std::runtime_error & exp)
    {
        std::cerr << exp.what() << std::endl;
        show_help(argv[0]);
        return EXIT_FAILURE;
    }
    catch (std::exception & exp)
    {
        std::cerr << exp.what() << std::endl;
        return EXIT_FAILURE;
    }
}
