﻿#pragma once
#include <vector>

/**
 * @brief utility functions for statistics.
 */
namespace stat_util
{

/**
 * @brief maximum
 */
double max(const std::vector<double> & data);

/**
 * @brief minimum
 */
double min(const std::vector<double> & data);

/**
 * @brief summation
 */
double sum(const std::vector<double> & data);

/**
 * @brief mean value
 */
double mean(const std::vector<double> & data);

/**
 * @brief variance
 */
double variance(const std::vector<double> & data);

/**
 * @brief Population standard deviation
 */
double stddev_p(const std::vector<double> & data);

/**
 * @brief median
 */
double median(const std::vector<double> & data);

} // namespace
