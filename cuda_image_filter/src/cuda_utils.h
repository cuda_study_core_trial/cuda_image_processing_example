﻿#pragma once

#include <stdexcept>
#include <cuda_runtime.h>

/**
 * @brief CUDAエラーを入れる例外クラス
 */
class MyCudaRuntimeError : public std::runtime_error
{
private:
    cudaError_t cudaError;

public:
    MyCudaRuntimeError(const cudaError_t _cudaError) :
        std::runtime_error(cudaGetErrorString(_cudaError)),
        cudaError(_cudaError)
        {}

    cudaError_t getCudaError() const { return cudaError; }
    const char * name() const { return cudaGetErrorName(cudaError); }
};

/**
 * @brief 切り上げ処理(ブロックサイズ算出用)
 * @param[in] value 処理対象
 * @param[in] unit 切り上げ単位
 * @return value をunitで切り上げた値
 */
inline uint32_t ceil_uint32(const uint32_t value, const uint32_t unit)
{
    return (value + unit - 1) / unit;
}

